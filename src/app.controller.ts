import {
  CacheInterceptor,
  Controller,
  Get,
  UseInterceptors,
  CACHE_MANAGER,Inject
} from '@nestjs/common';

@Controller()
@UseInterceptors(CacheInterceptor)
export class AppController {
  counter = 0;
  constructor(@Inject(CACHE_MANAGER) private cacheManager) {}

  // The first call increments to one, the preceding calls will be answered by the cache
  // without incrementing the counter. Only after you clear the cache by calling /reset
  // the counter will be incremented once again.
  @Get()
  @UseInterceptors(CacheInterceptor)
 async  incrementCounter() {
    // this.counter++;
    // return this.counter;
    console.log('llego')
     return  await new Promise((resolve) => 
     {
      setTimeout(() => {
        console.log('respondio')
        resolve(this.counter);
      }, 4000);
       }
     );

 
  }



  // Call this endpoint to reset the cache for the route '/'
  @Get('reset')
  resetCache() {
    const routeToClear = '/';
    this.cacheManager.del(routeToClear, () => console.log('clear done'));
  }
}
